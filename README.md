# Building a Website from Scratch with ExpressJS and Bootstrap

## A website using Bootstrap and ExpressJS. Express will work as the web server and routing handler, whereas we will use Bootstrap and basic HTML for the view.

Seen on: [www.codementor.io](https://www.codementor.io/codeforgeek/build-website-from-scratch-using-expressjs-and-bootstrap-du107sby7)


To get the website to work you will need to have node.js installed and then from within the project directory run the following commands in command prompt:

```npm install```

```npm start```

Then vissit ```localhost:3000``` and view your first website.
